

import java.io.*;
class IntegerDemo{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row:");
		int row=Integer.parseInt(br.readLine());
		char ch='A';
		for(int i=0;i<=row;i++){
		
			int num=1;
			for(int j=0;j<=i;j++){
				if(i%2==1){
					System.out.print(ch++  +" ");
				}
			
				
				else{
					System.out.print(num++ +" ");
					ch++;
				}
			}
				System.out.println();
		}
	}
}


